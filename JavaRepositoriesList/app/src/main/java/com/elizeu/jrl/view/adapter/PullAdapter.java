package com.elizeu.jrl.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.elizeu.jrl.R;
import com.elizeu.jrl.domain.Pull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by elizeucastro on 12/11/16.
 */

public class PullAdapter extends RecyclerView.Adapter<PullAdapter.ViewHolder> {

    public interface PullItemClickListener {

        void onPullItemClick(Pull pull);
    }

    private final Context context;
    private final PullItemClickListener listener;
    private List<Pull> pulls = new ArrayList<>();

    public PullAdapter(final Context context, PullItemClickListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_pull_list_item, parent, false);
        return new PullAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Pull pull = pulls.get(position);
        holder.txvPullTitle.setText(pull.getTitle());
        holder.txvPullBody.setText(pull.getBody());
        holder.txvPullOwner.setText(pull.getUser().getLogin());
        Glide.with(context).load(pull.getUser().getAvatarUrl()).into(holder.ivPullOwner);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                listener.onPullItemClick(pulls.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return pulls.size();
    }

    public void setPulls(final List<Pull> pulls) {
        this.pulls = pulls;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView txvPullTitle;
        final TextView txvPullBody;
        final TextView txvPullOwner;
        final ImageView ivPullOwner;
        final View itemView;

        ViewHolder(final View itemView) {
            super(itemView);
            txvPullTitle = (TextView) itemView.findViewById(R.id.txv_pull_title);
            txvPullBody = (TextView) itemView.findViewById(R.id.txv_pull_body);
            txvPullOwner = (TextView) itemView.findViewById(R.id.txv_pull_owner);
            ivPullOwner = (ImageView) itemView.findViewById(R.id.iv_pull_owner);
            this.itemView = itemView;
        }
    }
}
