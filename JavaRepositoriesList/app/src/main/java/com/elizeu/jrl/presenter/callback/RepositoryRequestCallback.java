package com.elizeu.jrl.presenter.callback;

import com.elizeu.jrl.domain.Repository;

/**
 * Created by elizeucastro on 12/11/16.
 */

public interface RepositoryRequestCallback extends RequestCallback {

    void onRepositoriesSuccess(Repository repository);

}
