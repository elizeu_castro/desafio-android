package com.elizeu.jrl.view.model;

import com.elizeu.jrl.domain.Pull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by elizeucastro on 12/11/16.
 */

public class PullModel extends AbstractModel {
    private String owner;
    private String repo;
    private int currentPage;
    private List<Pull> pulls = new ArrayList<>();

    public String getOwner() {
        return owner;
    }

    public void setOwner(final String owner) {
        this.owner = owner;
    }

    public String getRepo() {
        return repo;
    }

    public void setRepo(final String repo) {
        this.repo = repo;
    }

    public List<Pull> getPulls() {
        return pulls;
    }

    public void addPulls(final List<Pull> pulls) {
        this.pulls.addAll(pulls);
    }

    public void setCurrentPage(final int currentPage) {
        this.currentPage = currentPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int getCurrentSize() {
        return this.pulls.size();
    }

    public void resetPage() {
        currentPage = currentPage > 1 ? currentPage - 1 : currentPage;
    }

}
