package com.elizeu.jrl.domain;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by elizeucastro on 12/11/16.
 */

public class User implements Serializable {

    private Integer id;
    private String login;
    @SerializedName("avatar_url")
    private String avatarUrl;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(final String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
