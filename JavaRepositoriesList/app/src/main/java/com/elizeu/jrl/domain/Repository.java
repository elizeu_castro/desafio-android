package com.elizeu.jrl.domain;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by elizeucastro on 12/11/16.
 */

public class Repository implements Serializable {

    @SerializedName("total_count")
    private Integer totalCount;

    @SerializedName("items")
    private List<Item> items = new ArrayList<>();

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(final Integer totalCount) {
        this.totalCount = totalCount;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(final List<Item> items) {
        this.items = items;
    }
}
