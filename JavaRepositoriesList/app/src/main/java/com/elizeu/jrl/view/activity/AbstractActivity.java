package com.elizeu.jrl.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.elizeu.jrl.presenter.AbstractPresenter;
import com.elizeu.jrl.view.AbstractView;

public abstract class AbstractActivity extends AppCompatActivity implements AbstractView {

    @Override
    protected void onPostCreate(final Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getPresenter().onCreate(this, savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        getPresenter().onSaveInstanceState(outState);
    }

    protected abstract AbstractPresenter getPresenter();

}
