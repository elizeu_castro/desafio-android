package com.elizeu.jrl.domain;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by elizeucastro on 12/11/16.
 */

public class Pull implements Serializable {

    private Integer id;
    private Integer number;
    private String title;
    private String body;
    @SerializedName("created_at")
    private String date;
    private String state;
    private User user;
    @SerializedName("html_url")
    private String pullUrl;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(final Integer number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(final String body) {
        this.body = body;
    }

    public String getDate() {
        return date;
    }

    public void setDate(final String date) {
        this.date = date;
    }

    public String getState() {
        return state;
    }

    public void setState(final String state) {
        this.state = state;
    }

    public User getUser() {
        return user;
    }

    public void setUser(final User user) {
        this.user = user;
    }

    public String getPullUrl() {
        return pullUrl;
    }

    public void setPullUrl(final String pullUrl) {
        this.pullUrl = pullUrl;
    }
}
