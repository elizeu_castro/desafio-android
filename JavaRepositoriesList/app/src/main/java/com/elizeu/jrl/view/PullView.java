package com.elizeu.jrl.view;

import com.elizeu.jrl.domain.Pull;

import java.util.List;

/**
 * Created by elizeucastro on 12/11/16.
 */

public interface PullView extends AbstractView {

    void showPullRequests(List<Pull> pulls);

    void updateContainerStatus(int opened, int closed);

}