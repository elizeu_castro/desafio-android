package com.elizeu.jrl.view.listener;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by elizeucastro on 14/11/16.
 */

public abstract class EndlessScrollListener extends RecyclerView.OnScrollListener {

    private final LinearLayoutManager layoutManager;

    public EndlessScrollListener(final LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    @Override
    public void onScrolled(final RecyclerView recyclerView, final int dx, final int dy) {
        super.onScrolled(recyclerView, dx, dy);
        if (dy > 0 && this.layoutManager.findLastVisibleItemPosition() == this.layoutManager.getItemCount() - 1) {
            onNextPage();
        }
    }

    public abstract void onNextPage();

}
