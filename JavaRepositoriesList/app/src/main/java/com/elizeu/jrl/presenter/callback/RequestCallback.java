package com.elizeu.jrl.presenter.callback;

/**
 * Created by elizeucastro on 12/11/16.
 */

interface RequestCallback {

    void onFailure();
}
