package com.elizeu.jrl.service.request;

import com.elizeu.jrl.domain.Pull;
import com.elizeu.jrl.presenter.callback.PullRequestCallback;
import com.elizeu.jrl.service.enums.PullStatus;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by elizeucastro on 12/11/16.
 */

public class PullRequest {

    public void getPullRequests(final PullRequestCallback callback,
                                final String owner,
                                final String repo,
                                @PullStatus final String status,
                                final Integer page) {

        RequestManager.getGitHubApi()
                .getPullRequests(owner, repo, status, page)
                .enqueue(new Callback<List<Pull>>() {
                    @Override
                    public void onResponse(final Call<List<Pull>> call,
                                           final Response<List<Pull>> response) {
                        callback.onPullRequestsSuccess(response.body());
                    }

                    @Override
                    public void onFailure(final Call<List<Pull>> call,
                                          final Throwable t) {
                        callback.onFailure();
                    }
                });
    }
}
