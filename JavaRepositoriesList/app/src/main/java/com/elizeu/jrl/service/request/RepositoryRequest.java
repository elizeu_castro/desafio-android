package com.elizeu.jrl.service.request;

import com.elizeu.jrl.domain.Repository;
import com.elizeu.jrl.presenter.callback.RepositoryRequestCallback;
import com.elizeu.jrl.service.enums.SortOption;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by elizeucastro on 12/11/16.
 */

public class RepositoryRequest {

    public void getRepositories(final RepositoryRequestCallback callback,
                                final String language,
                                @SortOption final String sort,
                                final Integer page) {
        RequestManager.getGitHubApi()
                .getRepositoriesByLanguage(language, sort, page)
                .enqueue(new Callback<Repository>() {
                    @Override
                    public void onResponse(final Call<Repository> call, final Response<Repository> response) {
                        callback.onRepositoriesSuccess(response.body());
                    }

                    @Override
                    public void onFailure(final Call<Repository> call, final Throwable t) {
                        callback.onFailure();
                    }
                });
    }
}

