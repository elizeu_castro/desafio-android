package com.elizeu.jrl.presenter;

import com.elizeu.jrl.domain.Pull;
import com.elizeu.jrl.presenter.callback.PullRequestCallback;
import com.elizeu.jrl.service.enums.PullStatus;
import com.elizeu.jrl.service.request.PullRequest;
import com.elizeu.jrl.view.PullView;
import com.elizeu.jrl.view.model.PullModel;

import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

/**
 * Created by elizeucastro on 12/11/16.
 */

public class PullPresenter extends AbstractPresenter<PullView, PullModel>
        implements PullRequestCallback {

    private static final int MAX_SIZE = 1000;
    private static final int FIRST_PAGE = 1;
    private final PullRequest request;

    public PullPresenter(final String owner, final String repo) {
        super(new PullModel());
        request = new PullRequest();
        model.setOwner(owner);
        model.setRepo(repo);
    }

    @Override
    public void onViewPrepared(final boolean viewFirstTimeCreated) {
        view.bindViews();
        if (viewFirstTimeCreated) {
            retrievePullRequests(model.getOwner(), model.getRepo(), FIRST_PAGE);
        } else {
            view.showPullRequests(model.getPulls());
            countPullStatus();
        }
    }

    private void retrievePullRequests(final String owner, final String repo, final int page) {
        model.setCurrentPage(page);
        model.setLoading();
        view.showProgress();
        request.getPullRequests(this, owner, repo, PullStatus.ALL, page);
    }

    @Override
    public void onPullRequestsSuccess(final List<Pull> pulls) {
        if (null != pulls && CollectionUtils.isNotEmpty(pulls)) {
            model.addPulls(pulls);
            view.showPullRequests(pulls);
            countPullStatus();
        }
        model.setFinishLoading();
        view.hideProgress();
    }

    private void countPullStatus() {
        final List<Pull> pulls = model.getPulls();
        final int closed;
        int opened = 0;
        for (Pull pull : pulls) {
            if (pull.getState().equalsIgnoreCase("open")) {
                opened++;
            }
        }
        closed = pulls.size() - opened;
        view.updateContainerStatus(opened, closed);
    }

    @Override
    public void onFailure() {
        model.resetPage();
        model.setFinishLoading();
        view.hideProgress();
        view.showErrorScreen();
    }

    public void onNextPage() {
        if (model.isLoading()) {
            return;
        }
        if (model.getCurrentSize() < MAX_SIZE) {
            final int nextPage = model.getCurrentPage() + 1;
            retrievePullRequests(model.getOwner(), model.getRepo(), nextPage);
        }
    }
}
