package com.elizeu.jrl.view.model;

import com.elizeu.jrl.domain.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by elizeucastro on 12/11/16.
 */

public class RepositoryModel extends AbstractModel {

    private List<Item> repositories = new ArrayList<>();
    private int currentPage;
    private int totalCount;

    public void addRepositories(List<Item> repositories) {
        this.repositories.addAll(repositories);
    }

    public List<Item> getRepositories() {
        return repositories;
    }

    public void setCurrentPage(final int currentPage) {
        this.currentPage = currentPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setTotalCount(final int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public int getCurrentSize() {
        return this.repositories.size();
    }

    public void pageReset() {
        currentPage = currentPage > 1 ? currentPage - 1 : currentPage;
    }

}
