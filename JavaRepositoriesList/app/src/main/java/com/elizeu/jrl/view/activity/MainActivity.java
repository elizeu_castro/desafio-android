package com.elizeu.jrl.view.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.elizeu.jrl.R;
import com.elizeu.jrl.domain.Item;
import com.elizeu.jrl.presenter.AbstractPresenter;
import com.elizeu.jrl.presenter.RepositoryPresenter;
import com.elizeu.jrl.view.RepositoryView;
import com.elizeu.jrl.view.adapter.RepositoryAdapter;
import com.elizeu.jrl.view.listener.EndlessScrollListener;

import java.util.List;

public class MainActivity extends AbstractActivity implements
        RepositoryView, RepositoryAdapter.RepositoryItemClickListener {

    private RepositoryPresenter presenter;
    private RepositoryAdapter adapter;
    private ProgressDialog progress;
    private LinearLayout progressInLine;
    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new RepositoryPresenter();
        setContentView(R.layout.activity_main);
    }

    @Override
    protected AbstractPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void bindViews() {
        progressInLine = (LinearLayout) findViewById(R.id.ll_progress);
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        adapter = new RepositoryAdapter(getApplicationContext(), this);
        recyclerView.setAdapter(adapter);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(),
                linearLayoutManager.getOrientation()));
        recyclerView.addOnScrollListener(new EndlessScrollListener(linearLayoutManager) {
            @Override
            public void onNextPage() {
                presenter.onNextPage();
            }
        });
    }

    @Override
    public void showRepositories(final List<Item> repositories) {
        adapter.setRepositories(repositories);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void hideProgress() {
        if (null != progress) {
            progress.dismiss();
        }
        if (null != progressInLine) {
            progressInLine.setVisibility(View.GONE);
        }
    }

    @Override
    public void showProgress() {
        if (withoutPreviousData()) {
            showProgressDialog();
        } else {
            showProgressInLine();
        }
    }

    private void showProgressInLine() {
        progressInLine.setVisibility(View.VISIBLE);
    }

    private boolean withoutPreviousData() {
        return adapter.getItemCount() == 0;
    }

    private void showProgressDialog() {
        if (null == progress) {
            progress = new ProgressDialog(this);
            progress.setTitle(getString(R.string.app_name));
            progress.setMessage(getString(R.string.loadiong));
            progress.setCancelable(false);
        }
        progress.show();
    }

    @Override
    public void showErrorScreen() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(getString(R.string.error_java_repositories));
        builder.setCancelable(false);
        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onRepositoryItemClick(final Item repository) {
        final Intent intent = new Intent(getApplicationContext(), PullActivity.class);
        final Bundle bundle = new Bundle();
        bundle.putString(PullActivity.OWNER, repository.getOwner().getLogin());
        bundle.putString(PullActivity.REPO, repository.getName());
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        if (null != dialog) {
            dialog.cancel();
            dialog = null;
        }
        if (null != progress) {
            progress.dismiss();
            progress = null;
        }
        super.onDestroy();
    }
}
