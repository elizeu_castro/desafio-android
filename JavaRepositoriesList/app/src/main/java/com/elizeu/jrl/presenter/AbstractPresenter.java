package com.elizeu.jrl.presenter;

import android.os.Bundle;

import com.elizeu.jrl.view.AbstractView;
import com.elizeu.jrl.view.model.AbstractModel;

public abstract class AbstractPresenter<V extends AbstractView, M extends AbstractModel> {

    protected V view;
    protected M model;

    AbstractPresenter(final M model) {
        this.model = model;
    }

    public void onCreate(final AbstractView view, final Bundle bundle) {
        this.view = (V) view;
        onRestoreInstanceState(bundle);
        onViewPrepared(null == bundle);
    }

    public void onSaveInstanceState(final Bundle bundle) {
        if (null != bundle) {
            bundle.putSerializable(model.getBundleKey(), model);
        }
    }

    private void onRestoreInstanceState(final Bundle bundle) {
        if (null != bundle) {
            model = (M) bundle.getSerializable(model.getBundleKey());
        }
    }

    public abstract void onViewPrepared(boolean viewFirstTimeCreated);

}
