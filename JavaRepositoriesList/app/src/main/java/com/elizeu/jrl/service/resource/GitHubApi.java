package com.elizeu.jrl.service.resource;

import com.elizeu.jrl.domain.Pull;
import com.elizeu.jrl.domain.Repository;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by elizeucastro on 12/11/16.
 */

public interface GitHubApi {

    @GET("search/repositories")
    Call<Repository> getRepositoriesByLanguage(@Query("q") String language,
                                               @Query("sort") String sort,
                                               @Query("page") int page);

    @GET("repos/{owner}/{repo}/pulls")
    Call<List<Pull>> getPullRequests(@Path("owner") String owner,
                                     @Path("repo") String repo,
                                     @Query("state") String state,
                                     @Query("page") int page);
}
