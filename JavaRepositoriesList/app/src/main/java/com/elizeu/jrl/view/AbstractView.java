package com.elizeu.jrl.view;

/**
 * Created by elizeucastro on 12/11/16.
 */

public interface AbstractView {

    void bindViews();

    void hideProgress();

    void showProgress();

    void showErrorScreen();
}
