package com.elizeu.jrl.service.enums;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by elizeucastro on 12/11/16.
 */

@StringDef({SortOption.STARS, SortOption.FORKS, SortOption.UPDATED})
@Retention(RetentionPolicy.SOURCE)
public @interface SortOption {

    String STARS = "stars";
    String FORKS = "forks";
    String UPDATED = "updated";

}
