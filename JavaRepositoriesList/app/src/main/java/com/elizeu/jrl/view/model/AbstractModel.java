package com.elizeu.jrl.view.model;

import java.io.Serializable;

public abstract class AbstractModel implements Serializable {

    private boolean loading;

    public String getBundleKey() {
        return this.getClass().getName();
    }

    public void setLoading() {
        this.loading = true;
    }

    public void setFinishLoading() {
        this.loading = false;
    }

    public boolean isLoading() {
        return loading;
    }

}
