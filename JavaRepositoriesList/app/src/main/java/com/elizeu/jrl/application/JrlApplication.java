package com.elizeu.jrl.application;

import android.app.Application;

import com.elizeu.jrl.service.request.RequestManager;

/**
 * Created by elizeucastro on 14/11/16.
 */

public class JrlApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        RequestManager.start(getApplicationContext());
    }
}
