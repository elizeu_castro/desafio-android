package com.elizeu.jrl.view;

import com.elizeu.jrl.domain.Item;

import java.util.List;

/**
 * Created by elizeucastro on 12/11/16.
 */

public interface RepositoryView extends AbstractView {

    void showRepositories(List<Item> repositories);
}
