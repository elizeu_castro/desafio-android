package com.elizeu.jrl.service.request;

import android.content.Context;

import com.elizeu.jrl.service.resource.GitHubApi;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by elizeucastro on 14/11/16.
 */

public final class RequestManager {

    private static final String GIT_HUB_API = "https://api.github.com/";
    private static GitHubApi gitHubApi;

    public static void start(final Context context) {
        if (null == gitHubApi) {
            final Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(GIT_HUB_API)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .client(okHttpClient(context))
                    .build();
            gitHubApi = retrofit.create(GitHubApi.class);
        }
    }

    private static OkHttpClient okHttpClient(final Context context) {
        final int cacheSize = 10 * 1024;
        final OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.cache(new Cache(context.getCacheDir(), cacheSize));
        return client.build();
    }

    static GitHubApi getGitHubApi() {
        return gitHubApi;
    }
}
