package com.elizeu.jrl.presenter;

import com.elizeu.jrl.domain.Item;
import com.elizeu.jrl.domain.Repository;
import com.elizeu.jrl.presenter.callback.RepositoryRequestCallback;
import com.elizeu.jrl.service.enums.SortOption;
import com.elizeu.jrl.service.request.RepositoryRequest;
import com.elizeu.jrl.view.RepositoryView;
import com.elizeu.jrl.view.model.RepositoryModel;

import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

/**
 * Created by elizeucastro on 12/11/16.
 */

public class RepositoryPresenter extends AbstractPresenter<RepositoryView, RepositoryModel>
        implements RepositoryRequestCallback {

    private RepositoryRequest request;
    private static final String LANGUAGE_PARAMETER = "language:Java";
    private static final int MAX_SIZE = 1000;
    private static final int FIRST_PAGE = 1;

    public RepositoryPresenter() {
        super(new RepositoryModel());
        request = new RepositoryRequest();
    }

    @Override
    public void onViewPrepared(final boolean viewFirstTimeCreated) {
        view.bindViews();
        if (viewFirstTimeCreated) {
            retrieveRepositories(FIRST_PAGE);
        } else {
            view.showRepositories(model.getRepositories());
        }
    }

    public void onNextPage() {
        if (model.isLoading()){
            return;
        }
        final int totalCount = model.getTotalCount();
        final int currentSize = model.getCurrentSize();
        if (currentSize < totalCount && currentSize < MAX_SIZE) {
            final int nextPage = model.getCurrentPage() + 1;
            retrieveRepositories(nextPage);
        }
    }

    private void retrieveRepositories(final int page) {
        model.setLoading();
        model.setCurrentPage(page);
        view.showProgress();
        request.getRepositories(this, LANGUAGE_PARAMETER, SortOption.STARS, page);
    }

    @Override
    public void onRepositoriesSuccess(final Repository repository) {
        if (null != repository && CollectionUtils.isNotEmpty(repository.getItems())) {
            final List<Item> repositories = repository.getItems();
            model.setTotalCount(repository.getTotalCount());
            model.addRepositories(repositories);
            view.showRepositories(model.getRepositories());
        }
        model.setFinishLoading();
        view.hideProgress();
    }

    @Override
    public void onFailure() {
        model.setFinishLoading();
        model.pageReset();
        view.hideProgress();
        view.showErrorScreen();
    }

}
