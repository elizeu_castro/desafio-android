package com.elizeu.jrl.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.elizeu.jrl.R;
import com.elizeu.jrl.domain.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by elizeucastro on 12/11/16.
 */

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.ViewHolder> {

    public interface RepositoryItemClickListener {

        void onRepositoryItemClick(Item repository);

    }

    private final Context context;
    private final RepositoryItemClickListener listener;
    private List<Item> repositories = new ArrayList<>();

    public RepositoryAdapter(final Context context, final RepositoryItemClickListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_repository_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Item repository = repositories.get(position);

        holder.txvRepoTitle.setText(repository.getName());
        holder.txvRepoBody.setText(repository.getDescription());
        holder.txvOwner.setText(repository.getOwner().getLogin());
        Glide.with(context).load(repository.getOwner().getAvatarUrl()).into(holder.ivOwner);
        holder.txvFork.setText(String.valueOf(repository.getForksCount()));
        holder.txvStar.setText(String.valueOf(repository.getStargazersCount()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                listener.onRepositoryItemClick(repositories.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    public void setRepositories(final List<Item> repositories) {
        this.repositories = repositories;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView txvRepoTitle;
        TextView txvRepoBody;
        TextView txvOwner;
        TextView txvFork;
        TextView txvStar;
        ImageView ivOwner;
        View itemView;

        ViewHolder(final View itemView) {
            super(itemView);
            txvRepoTitle = (TextView) itemView.findViewById(R.id.txv_repo_title);
            txvRepoBody = (TextView) itemView.findViewById(R.id.txv_repo_body);
            txvOwner = (TextView) itemView.findViewById(R.id.txv_repo_owner);
            txvFork = (TextView) itemView.findViewById(R.id.txv_fork);
            txvStar = (TextView) itemView.findViewById(R.id.txv_star);
            ivOwner = (ImageView) itemView.findViewById(R.id.iv_repo_owner);
            this.itemView = itemView;
        }

    }
}
