package com.elizeu.jrl.view.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.elizeu.jrl.R;
import com.elizeu.jrl.domain.Pull;
import com.elizeu.jrl.presenter.AbstractPresenter;
import com.elizeu.jrl.presenter.PullPresenter;
import com.elizeu.jrl.view.PullView;
import com.elizeu.jrl.view.adapter.PullAdapter;
import com.elizeu.jrl.view.listener.EndlessScrollListener;

import java.util.List;
import java.util.Locale;

public class PullActivity extends AbstractActivity
        implements PullView, PullAdapter.PullItemClickListener {

    public static final String OWNER = "OWNER";
    public static final String REPO = "REPO";
    private PullPresenter presenter;
    private PullAdapter adapter;
    private ProgressDialog progressDialog;
    private LinearLayout llProgress;
    private TextView txvOpened;
    private TextView txvClosed;
    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull);

        if (null != getIntent() && null != getIntent().getExtras()) {

            final Bundle bundle = getIntent().getExtras();
            final String owner = bundle.getString(OWNER);
            final String repo = bundle.getString(REPO);
            setupActionBar(owner, repo);
            presenter = new PullPresenter(owner, repo);
        }

    }

    public void setupActionBar(final String owner, final String repo) {
        final ActionBar actionBar = getSupportActionBar();
        if (null == actionBar) {
            return;
        }
        actionBar.setTitle(getString(
                R.string.pull_screen_title,
                owner.toLowerCase(Locale.getDefault()),
                repo.toLowerCase(Locale.getDefault())));
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    @Override
    protected AbstractPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void bindViews() {
        txvOpened = (TextView) findViewById(R.id.txv_pull_opened);
        txvClosed = (TextView) findViewById(R.id.txv_pull_closed);
        llProgress = (LinearLayout) findViewById(R.id.ll_progress);
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_pulls);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(),
                linearLayoutManager.getOrientation()));
        adapter = new PullAdapter(this, this);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new EndlessScrollListener(linearLayoutManager) {
            @Override
            public void onNextPage() {
                presenter.onNextPage();
            }
        });
    }

    @Override
    public void showPullRequests(final List<Pull> pulls) {
        adapter.setPulls(pulls);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void updateContainerStatus(final int opened, final int closed) {
        txvOpened.setText(getString(R.string.pull_opened, opened));
        txvClosed.setText(getString(R.string.pull_closed, closed));
    }

    @Override
    public void hideProgress() {
        if (null != progressDialog) {
            progressDialog.dismiss();
        }
        if (null != llProgress) {
            llProgress.setVisibility(View.GONE);
        }
    }

    @Override
    public void showProgress() {
        if (withoutPreviousData()) {
            showProgressDialog();
        } else {
            showProgressInLine();
        }
    }

    private void showProgressInLine() {
        llProgress.setVisibility(View.VISIBLE);
    }

    private boolean withoutPreviousData() {
        return adapter.getItemCount() == 0;
    }

    private void showProgressDialog() {
        if (null == progressDialog) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle(getString(R.string.pull_requests));
            progressDialog.setMessage(getString(R.string.loadiong));
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
    }

    @Override
    public void showErrorScreen() {
        AlertDialog.Builder builder = new AlertDialog.Builder(PullActivity.this);
        builder.setMessage(getString(R.string.error_pull_requests));
        builder.setCancelable(false);
        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onPullItemClick(final Pull pull) {
        Uri uri = Uri.parse(pull.getPullUrl());
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        if (null != dialog) {
            dialog.cancel();
            dialog = null;
        }
        if (null != progressDialog) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        super.onDestroy();
    }

}