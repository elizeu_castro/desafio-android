package com.elizeu.jrl.service.enums;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by elizeucastro on 12/11/16.
 */

@StringDef({PullStatus.OPEN, PullStatus.CLOSED, PullStatus.ALL})
@Retention(RetentionPolicy.SOURCE)
public @interface PullStatus {

    String OPEN = "open";
    String CLOSED = "closed";
    String ALL = "all";

}
