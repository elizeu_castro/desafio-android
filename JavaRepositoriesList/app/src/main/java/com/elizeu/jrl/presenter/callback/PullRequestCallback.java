package com.elizeu.jrl.presenter.callback;

import com.elizeu.jrl.domain.Pull;

import java.util.List;

/**
 * Created by elizeucastro on 12/11/16.
 */

public interface PullRequestCallback extends RequestCallback {

    void onPullRequestsSuccess(List<Pull> pulls);
}
