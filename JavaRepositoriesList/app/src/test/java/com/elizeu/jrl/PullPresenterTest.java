package com.elizeu.jrl;

import com.elizeu.jrl.domain.Pull;
import com.elizeu.jrl.presenter.PullPresenter;
import com.elizeu.jrl.service.enums.PullStatus;
import com.elizeu.jrl.service.request.PullRequest;
import com.elizeu.jrl.view.PullView;
import com.elizeu.jrl.view.model.PullModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.util.reflection.Whitebox.setInternalState;

/**
 * Created by elizeucastro on 13/11/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class PullPresenterTest {

    private static final String OWNER = "elizeu";
    private static final String REPO = "teste";
    private static final int FIRST_PAGE = 1;

    @Mock
    private PullView view;

    @Mock
    private PullRequest request;

    @Mock
    private PullModel model;

    private PullPresenter presenter;

    @Before
    public void setup() {
        presenter = new PullPresenter(OWNER, REPO);

        setInternalState(presenter, "view", view);
        setInternalState(presenter, "request", request);
    }

    @Test
    public void onViewPreparedFirstAccessTest() {
        presenter.onViewPrepared(true);
        verify(view).bindViews();
        verify(view).showProgress();
        verify(request).getPullRequests(presenter, OWNER, REPO, PullStatus.ALL, FIRST_PAGE);
    }

    @Test
    public void onViewPreparedRestoreInstanceTest() {
        presenter.onViewPrepared(false);
        verify(view).bindViews();
        verify(view).showPullRequests(anyList());
    }

    @Test
    public void onPullRequestsSuccessTest() {
        List<Pull> pulls = new ArrayList<>();
        pulls.add(new Pull());
        setInternalState(presenter, "model", model);

        presenter.onPullRequestsSuccess(pulls);

        verify(model).addPulls(pulls);
        verify(view).showPullRequests(pulls);
        verify(view).hideProgress();
    }

    @Test
    public void onFailureTest() {
        presenter.onFailure();
        view.hideProgress();
        view.showErrorScreen();
    }

    @Test
    public void onNextPageTest() {
        setInternalState(presenter, "model", model);

        when(model.getCurrentPage()).thenReturn(2);
        when(model.getCurrentSize()).thenReturn(50);
        when(model.getOwner()).thenReturn(OWNER);
        when(model.getRepo()).thenReturn(REPO);

        presenter.onNextPage();

        verify(model).setCurrentPage(3);
        verify(view).showProgress();
        verify(model).setLoading();
        verify(request).getPullRequests(presenter, OWNER, REPO, PullStatus.ALL, 3);

    }

}
