package com.elizeu.jrl;

import com.elizeu.jrl.domain.Item;
import com.elizeu.jrl.domain.Repository;
import com.elizeu.jrl.presenter.RepositoryPresenter;
import com.elizeu.jrl.service.enums.SortOption;
import com.elizeu.jrl.service.request.RepositoryRequest;
import com.elizeu.jrl.view.RepositoryView;
import com.elizeu.jrl.view.model.RepositoryModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.internal.util.reflection.Whitebox.setInternalState;

/**
 * Created by elizeucastro on 13/11/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class RepositoryPresenterTest {

    private static final String LANGUAGE_PARAMETER = "language:Java";

    @Mock
    private RepositoryView view;

    @Mock
    private RepositoryRequest request;

    @Mock
    private RepositoryModel model;

    private RepositoryPresenter presenter;

    @Before
    public void setup() {
        presenter = new RepositoryPresenter();
        setInternalState(presenter, "view", view);
        setInternalState(presenter, "request", request);
    }

    @Test
    public void onViewPreparedFirstAccessTest() {
        setInternalState(presenter, "model", model);
        presenter.onViewPrepared(true);
        verify(view).bindViews();
        verify(view).showProgress();
        verify(model).setCurrentPage(1);
        verify(model).setLoading();
        verify(request).getRepositories(presenter, LANGUAGE_PARAMETER, SortOption.STARS, 1);
    }

    @Test
    public void onViewPreparedRestoreInstanceTest() {
        presenter.onViewPrepared(false);
        verify(view).bindViews();
        verify(view).showRepositories(anyList());
    }

    @Test
    public void onRepositoriesSuccessTest() {
        Repository repository = new Repository();
        repository.setTotalCount(1);
        List<Item> items = new ArrayList<>();
        items.add(new Item());
        repository.setItems(items);

        setInternalState(presenter, "model", model);
        presenter.onRepositoriesSuccess(repository);

        verify(model).setTotalCount(repository.getTotalCount());
        verify(model).addRepositories(repository.getItems());
        verify(model).setFinishLoading();
        verify(view).showRepositories(model.getRepositories());
        verify(view).hideProgress();
    }

    @Test
    public void onFailureTest() {
        setInternalState(presenter, "model", model);
        presenter.onFailure();
        verify(model).setFinishLoading();
        verify(model).pageReset();
        verify(view).hideProgress();
        verify(view).showErrorScreen();
    }

    @Test
    public void onNextPageTest() {
        setInternalState(presenter, "model", model);

        when(model.getCurrentPage()).thenReturn(2);
        when(model.getTotalCount()).thenReturn(10);
        when(model.getCurrentSize()).thenReturn(1);

        presenter.onNextPage();

        verify(model).setCurrentPage(3);
        verify(view).showProgress();
        verify(model).setLoading();
        verify(request).getRepositories(presenter, LANGUAGE_PARAMETER, SortOption.STARS, 3);

    }

    @Test
    public void onNextPageOnLastPageTest() {
        setInternalState(presenter, "model", model);

        when(model.getCurrentPage()).thenReturn(5);
        when(model.getTotalCount()).thenReturn(10);
        when(model.getCurrentSize()).thenReturn(10);

        presenter.onNextPage();

        verifyNoMoreInteractions(view);
        verifyNoMoreInteractions(request);

    }
}
